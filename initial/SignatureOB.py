#python 3
import sys, os, base64, datetime, hashlib, hmac 

def sign(key, msg):
    return hmac.new(key, msg.encode('utf-8'), hashlib.sha256).digest()

def signhex(key, msg):
    return hmac.new(key, msg.encode('utf-8'), hashlib.sha256).hexdigest()

def getSignature(key,uuid, dateStamp, apiKey, strToSign):
    kSecret = sign(('TCRB1' + key).encode('utf-8'), dateStamp)
    kUUID = sign(kSecret, uuid)
    kApiKey = sign(kUUID, apiKey)
    kSigning = sign(kApiKey, 'tcrb1_request')
    kSignature = signhex(kSigning,strToSign)

    return kSignature

#t = datetime.datetime.utcnow()
#amz_date = t.strftime('%Y%m%dT%H%M%SZ')
#date_stamp = t.strftime('%Y%m%d')
#key = 'bf850aef8cbc94bff6a12226a0acfd7acc109fde2f9b0934b233ab759423fbf4'
#uuid = 'd4acfe37-edd8-4516-bea9-cc3f1ef2c503'
#dateStamp = '1588152809'
#apiKey = '1191896e6ef920430cfe9fc7682a88ec236780d95c67f4503661c610853fe56a'
#Method = 'GET'
#CanonicalURI = '/api/binding/v1/binding'
#strToSign = Method+CanonicalURI+uuid
#print(getSignature(key,uuid, dateStamp, apiKey, strToSign))
