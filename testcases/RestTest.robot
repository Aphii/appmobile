*** Settings ***
Library           REST    https://api-preprod.onlinebanking-privatepartner.com    ssl_verify=false
Library           String
Library           DateTime
Library           ../Python/SignatureOB.py

*** Variables ***
${APIKey}         38fb577051edf6b613c9dbcdf1c40e6c190084b04c45f28f8f5e833d0ddb91f0
${secretKey}      475sw39nulg8uv4lte0iv4zlir5pctzgwtoo

*** Test Cases ***
RestTest
    RegOTP

*** Keywords ***
UUID
    ${id}=    Evaluate    str(uuid.uuid4())
    [Return]    ${id}

UnixTimeStamp
    ${datetime}    Get Current Date
    log    ${datetime}
    ${UTC}=    Convert Date    ${datetime}    exclude_millis=No
    ${UTC}=    Convert Date    ${UTC}    epoch
    ${UTC}=    Convert To String    ${UTC}
    ${UTC}=    String.Get Substring    ${UTC}    \    -4
    ${cDate}=    Convert Date    ${datetime}    exclude_millis=No    result_format=%Y-%m-%dt%H:%M:%Sz
    [Return]    ${UTC}    ${cDate}

Signature
    [Arguments]    ${secretKey}    ${id}    ${UnixTimeStamp}    ${APIKey}    ${StringToSign}
    ${signature}=    Get Signature    ${secretKey}    ${id}    ${UnixTimeStamp}    ${APIKey}    ${StringToSign}
    [Return]    ${signature}

RegOTP
    ${uuid}=    UUID
    ${timeStamp}=    UnixTimeStamp
    ${method}    Set Variable    POST
    ${CanonicalURI}    Set Variable    /api/otp/v1/otp
    ${apiUUID}    Set Variable    ${uuid}${timeStamp[0]}
    ${stringToSign}=    Set Variable    ${method}${CanonicalURI}${uuid}
    ${signature}=    Signature    ${secretKey}    ${uuid}    ${timeStamp[0]}    ${APIKey}    ${stringToSign}
    POST    endpoint=${CanonicalURI}    body={"accountRefId": "2cc4cd9ef74b4cc6a3aab9d8fb35b0a84ac8ef274068cc0eb783c01f", "partnerRefId": "Automate_Site4", "transactionRefNo": "RegrRelease_20201026_0001", "requestId": "0001", "requestDateTime": "timeStamp[1]"}    headers={ "x-api-key": "${APIKey}", "x-api-signature":"${signature}", "x-api-uuidv4":"${apiUUID}","x-apigw-api-id":"jheyld84sj" ,"x-api-language":"EN","Content-Type":"application/json"}
    Output    response body
