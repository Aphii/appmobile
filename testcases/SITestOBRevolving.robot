*** Settings ***
Library           SeleniumLibrary
Library           String
Library           DateTime
Library           ../Python/SignatureOB.py
Library           DatabaseLibrary
Library           RequestsLibrary
Library           ../Python/ExcelLib.py

*** Variables ***
${APIKey}         38fb577051edf6b613c9dbcdf1c40e6c190084b04c45f28f8f5e833d0ddb91f0
${secretKey}      475sw39nulg8uv4lte0iv4zlir5pctzgwtoo
${URL_ReqPreProd}    https://api-preprod.onlinebanking-privatepartner.com
${otp}            ${EMPTY}

*** Test Cases ***
Test
    ${pac}=    RegOTP
    Sleep    2
    GetSMS    ${pac}
    ${otpVal}=    Convert To String    ${otp[0]}
    ${otpVal}=    Get Substring    ${otpVal}    14    2 0
    Comment    ${token}=    VerifyOTP    ${pac}    ${otpVal}
    Given    ${pac}=    RegOTP

*** Keywords ***
UnixTimeStamp
    ${datetime}    Get Current Date
    log    ${datetime}
    ${UTC}=    Convert Date    ${datetime}    exclude_millis=No
    ${UTC}=    Convert Date    ${UTC}    epoch
    ${UTC}=    Convert To String    ${UTC}
    ${UTC}=    String.Get Substring    ${UTC}    \    -4
    ${cDate}=    Convert Date    ${datetime}    exclude_millis=No    result_format=%Y-%m-%dt%H:%M:%Sz
    [Return]    ${UTC}    ${cDate}

UUID
    ${id}=    Evaluate    str(uuid.uuid4())
    [Return]    ${id}

Signature
    [Arguments]    ${secretKey}    ${id}    ${UnixTimeStamp}    ${APIKey}    ${StringToSign}
    ${signature}=    Get Signature    ${secretKey}    ${id}    ${UnixTimeStamp}    ${APIKey}    ${StringToSign}
    [Return]    ${signature}

RegOTP
    ${uuid}=    UUID
    ${timeStamp}=    UnixTimeStamp
    ${method}    Set Variable    POST
    ${CanonicalURI}    Set Variable    /api/otp/v1/otp
    ${apiUUID}    Set Variable    ${uuid}${timeStamp[0]}
    Log    ${APIKey}
    Log    ${secretKey}
    ${stringToSign}=    Set Variable    ${method}${CanonicalURI}${uuid}
    ${signature}=    Signature    ${secretKey}    ${uuid}    ${timeStamp[0]}    ${APIKey}    ${stringToSign}
    Create Session    ReqOTP    ${URL_ReqPreProd}
    &{data}=    Create Dictionary    accountRefId=2cc4cd9ef74b4cc6a3aab9d8fb35b0a84ac8ef274068cc0eb783c01f    partnerRefId=Automate_Site4    transactionRefNo=RegrRelease_20201026_0001    requestId=0001    requestDateTime=${timeStamp[1]}
    &{header}=    Create Dictionary    x-api-key=${APIKey}    x-api-signature=${signature}    x-api-uuidv4=${apiUUID}    x-apigw-api-id=jheyld84sj    x-api-language=EN    Content-Type=application/json
    ${resp}=    Post Request    ReqOTP    ${CanonicalURI}    data=${data}    headers=${header}
    &{resu}=    Evaluate    ${resp.text}
    ${pacCode}    Set Variable    ${resu['responseData']['pacCode']}
    [Return]    ${pacCode}

GetSMS
    [Arguments]    ${PacCode}
    Connect To Database    psycopg2    api_db    readonly    egdeapi    192.168.224.116    5432
    ${otp}=    Query    SELECT message From api.true_sms_log WHERE message LIKE '%${PacCode}%'
    Log    ${otp}
    Set Global Variable    ${otp}    ${otp}
    Disconnect From Database
    Run Keyword If    ${otp}==[]    GetSMS2    ${PacCode}

GetSMS2
    [Arguments]    ${PacCode}
    Connect To Database    psycopg2    api_db    readonly    egdeapi    192.168.224.117    5432
    ${otp}=    Query    SELECT message From api.true_sms_log WHERE message LIKE '%${PacCode}%'
    Log    ${otp}
    Set Global Variable    ${otp}    ${otp}
    Disconnect From Database

VerifyOTP
    [Arguments]    ${pacCode}    ${otpVal}
    ${uuid}=    UUID
    ${timeStamp}=    UnixTimeStamp
    ${method}    Set Variable    PUT
    ${CanonicalURI}    Set Variable    /api/otp/v1/otp
    ${apiUUID}    Set Variable    ${uuid}${timeStamp[0]}
    Log    ${APIKey}
    Log    ${secretKey}
    ${stringToSign}=    Set Variable    ${method}${CanonicalURI}${uuid}
    ${signature}=    Signature    ${secretKey}    ${uuid}    ${timeStamp[0]}    ${APIKey}    ${stringToSign}
    Create Session    VerOTP    ${URL_ReqPreProd}
    &{data}=    Create Dictionary    accountRefId=703b94cbe4601256e103aa9f728dac638e58ab3bfbaf5692a847b860    partnerRefId=Automate_Regression_02    pacCode=${pacCode}    otpValue=${otpVal}    transactionRefNo=RegrRelease_20201026_0001    requestId=0001    requestDateTime=${timeStamp[1]}
    &{header}=    Create Dictionary    x-api-key=${APIKey}    x-api-signature=${signature}    x-api-uuidv4=${apiUUID}    x-apigw-api-id=fhvn2malh5    x-api-language=EN    Content-Type=application/json
    ${resp}=    Put Request    VerOTP    ${CanonicalURI}    data=${data}    headers=${header}
    &{resu}=    Evaluate    ${resp.text}
    ${otpToken}    Set Variable    ${resu['responseData']['otpToken']}
    [Return]    ${otpToken}

LoanInquiry
    ${uuid}=    UUID
    ${timeStamp}=    UnixTimeStamp
    ${method}    Set Variable    POST
    ${CanonicalURI}    Set Variable    /api/partnerauthorizedata/v1/loanaccount
    ${apiUUID}    Set Variable    ${uuid}${timeStamp[0]}
    Log    ${APIKey}
    Log    ${secretKey}
    ${stringToSign}=    Set Variable    ${method}${CanonicalURI}${uuid}
    ${signature}=    Signature    ${secretKey}    ${uuid}    ${timeStamp[0]}    ${APIKey}    ${stringToSign}
    Create Session    Inquiry    ${URL_ReqPreProd}
    &{data}=    Create Dictionary    accountRefId=7ba6e099882a6aa122f9437db37a08df307cadb7f975d40bcb529945    partnerRefId=Automate_Regression_02    transactionRefNo=RegrRelease_20201026_0001    requestId=0001    requestDateTime=${timeStamp[1]}
    &{header}=    Create Dictionary    x-api-key=${APIKey}    x-api-signature=${signature}    x-api-uuidv4=${apiUUID}    x-apigw-api-id=80toolta95    x-api-language=EN    Content-Type=application/json
    ${resp}=    Post Request    Inquiry    ${CanonicalURI}    data=${data}    headers=${header}
    &{resu}=    Evaluate    ${resp.text}
    Comment    ${otpToken}    Set Variable    ${resu['responseData']['otpToken']}

Ebill
    ${uuid}=    UUID
    ${timeStamp}=    UnixTimeStamp
    ${method}    Set Variable    POST
    ${CanonicalURI}    Set Variable    /api/partnerauthorizedata/v1/ebill
    ${apiUUID}    Set Variable    ${uuid}${timeStamp[0]}
    Log    ${APIKey}
    Log    ${secretKey}
    ${stringToSign}=    Set Variable    ${method}${CanonicalURI}${uuid}
    ${signature}=    Signature    ${secretKey}    ${uuid}    ${timeStamp[0]}    ${APIKey}    ${stringToSign}
    Create Session    Inquiry    ${URL_ReqPreProd}
    &{data}=    Create Dictionary    accountRefId=7ba6e099882a6aa122f9437db37a08df307cadb7f975d40bcb529945    partnerRefId=Automate_Regression_02    transactionRefNo=RegrRelease_20201026_0001    requestId=0001    requestDateTime=${timeStamp[1]}
    &{header}=    Create Dictionary    x-api-key=${APIKey}    x-api-signature=${signature}    x-api-uuidv4=${apiUUID}    x-apigw-api-id=80toolta95    x-api-language=EN    Content-Type=application/json
    ${resp}=    Post Request    Inquiry    ${CanonicalURI}    data=${data}    headers=${header}
    &{resu}=    Evaluate    ${resp.text}
    Comment    ${otpToken}    Set Variable    ${resu['responseData']['otpToken']}
